import React, { useState } from 'react';
import YandexMapPick from '../components/YandexMapPick/YandexMapPick';
import styles from './Page.module.scss'
import Modal from '../components/Modal/Modal';

const Page = () => {
    const [center, setCenter] = useState([55.755819, 37.617644])
    const [mark, setMark] = useState(null)
    const [place, setPlace] = useState(null)
    const [open, setOpen] = useState(false)
    const [] = useState()

    const onClose = () => {
        setOpen(false)
    }

    const handleClickMap = (place) => {
        setPlace(place)
        setOpen(true)
    }

    return (
        <div className="">
            <div className={styles.wrapper}>
                <YandexMapPick
                    center={center}
                    mark={mark}
                    setMark={setMark}
                    setPlace={setPlace}
                    handleClick={handleClickMap}
                    setCenter={setCenter}
                />
            </div>
            {<Modal
                duration={300}
                open={open}
                onClose={onClose}
                title={"Когда приступить к работе?"}
                closeOnOverlayClick
                size='small'
            >
                {place}
            </Modal>}
        </div>
    )
}

export default Page