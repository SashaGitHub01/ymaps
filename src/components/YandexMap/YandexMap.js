import React, { useEffect } from 'react'
import { YMaps, Map, Placemark, FullscreenControl, ZoomControl, TrafficControl, GeolocationControl, SearchControl } from 'react-yandex-maps';

const YandexMap = ({center, marks}) => {

    return (
        <YMaps 
            query={{
                apikey: 'e4963ec0-43ab-40c8-85ad-30043e1d83a7',
            }} 
        >
            <Map
                state={{center, zoom: 9}}
                options={{
                    suppressMapOpenBlock: true
                }}
                height='80vh'
                width="100%"
            >
            {marks.map((mark, i) => (
                <Placemark
                    key={i}
                    geometry={mark.coords}
                    options={{
                        iconColor: 'red',
                        preset: 'islands#dotIcon',
                        balloonAutoPanMargin: ['50', '50'],
                    }}
                    properties={{
                        balloonContentHeader: `${mark.name}`,
                        balloonContentBody: `Body`,
                        balloonContentFooter: `Footer`,
                    }}
                    modules={["geoObject.addon.balloon"]}
                />
            ))}
            <FullscreenControl/>
            <ZoomControl/>
            <GeolocationControl/>
            <SearchControl/>
            </Map>
        </YMaps>
    )
}

export default YandexMap