import { action } from '@storybook/addon-actions';
import Modal from '../Modal'
import { YMaps, Map } from 'react-yandex-maps'
import styles from './Modal.module.scss'
import { useArgs } from '@storybook/client-api';

const lorem = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, velit blanditiis. Iste, harum. Rerum ullam ipsam enim cupiditate suscipit minus repudiandae? A, quibusdam qui quidem excepturi dolorum nostrum aut error!';
const largeText = (
    <div style={{ display: 'flex', flexDirection: 'column', gap: '50px', fontSize: '25px' }}>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
        <div>{lorem}</div>
    </div>
)
const mapAndImages = (
    <>
        <YMaps>
            <Map state={{ center: [55.0255, 25.512], zoom: 9 }} width={'100%'} />
        </YMaps><div className={styles.grid}>
            <img src='https://res.cloudinary.com/twitter-uploads/image/upload/v1638537457/sample.jpg' />
            <img src='https://res.cloudinary.com/twitter-uploads/image/upload/v1638537457/sample.jpg' />
            <img src='https://res.cloudinary.com/twitter-uploads/image/upload/v1638537457/sample.jpg' />
            <img src='https://res.cloudinary.com/twitter-uploads/image/upload/v1638537457/sample.jpg' />
            <img src='https://res.cloudinary.com/twitter-uploads/image/upload/v1638537457/sample.jpg' />
            <img src='https://res.cloudinary.com/twitter-uploads/image/upload/v1638537457/sample.jpg' />
            <img src='https://res.cloudinary.com/twitter-uploads/image/upload/v1638537457/sample.jpg' />
            <img src='https://res.cloudinary.com/twitter-uploads/image/upload/v1638537457/sample.jpg' />
        </div>
    </>
)

export default {
    title: 'UI/Modal',
    component: Modal,
    args: {
        children: 'Content',
        title: 'Title',
        open: false
    },
    argTypes: {
        size: {
            defaultValue: 'small',
            options: ['small', 'medium', 'fullscreen'],
            control: {
                type: 'radio'
            }
        },

        duration: {
            type: 'number',
            defaultValue: 250
        },

        closeOnOverlayClick: {
            type: 'boolean',
            defaultValue: true
        },
    }
}

export const Playground = ({ onClose, ...args }) => {
    const [{ open }, updateArgs] = useArgs();

    const toggleModal = () => updateArgs({ open: !open });

    return (
        <div className="">
            <button onClick={toggleModal}>
                OPEN MODAL
            </button>
            <Modal
                onClose={toggleModal}
                {...args}
            />
        </div>
    )
}

const Template = (args) => <Modal {...args} />

export const Default = Template.bind({})
Default.args = {
    children: lorem,
    open: true,
    title: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, velit blanditiis. Nobis, velitvelit',
}

// large text
export const WidthLargeText = Template.bind({})
WidthLargeText.args = {
    children: largeText,
    open: true,
    title: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, velit blanditiis. Nobis, velitvelit',
}

export const SmallWidthLargeText = Template.bind({})
SmallWidthLargeText.args = {
    children: largeText,
    open: true,
    size: 'small',
    title: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, velit blanditiis. Nobis, velitvelit',
}

export const MediumWidthLargeText = Template.bind({})
MediumWidthLargeText.args = {
    children: largeText,
    open: true,
    size: 'medium',
    title: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, velit blanditiis. Nobis, velitvelit',
}

export const FullscreenWidthLargeText = Template.bind({})
FullscreenWidthLargeText.args = {
    children: largeText,
    open: true,
    size: 'fullscreen',
    title: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, velit blanditiis. Nobis, velitvelit',
}

//map and img
export const SmallWidthImgAndMap = Template.bind({})
SmallWidthImgAndMap.args = {
    children: mapAndImages,
    open: true,
    size: 'small',
    title: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, velit blanditiis. Nobis, velitvelit',
}

export const MediumWidthImgAndMap = Template.bind({})
MediumWidthImgAndMap.args = {
    children: mapAndImages,
    open: true,
    size: 'medium',
    title: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, velit blanditiis. Nobis, velitvelit',
}

export const FullscreenWidthImgAndMap = Template.bind({})
FullscreenWidthImgAndMap.args = {
    children: mapAndImages,
    open: true,
    size: 'fullscreen',
    title: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis, velit blanditiis. Nobis, velitvelit',
}
