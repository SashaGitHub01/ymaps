import classNames from 'classnames'
import React, { useEffect, useState } from 'react'
import ReactModal from 'react-modal';
import styles from './Modal.module.scss'

const Modal = ({ children, open, onClose, title, duration = 200, closeOnOverlayClick = true, size = 'small' }) => {

    return (
        <ReactModal
            isOpen={open}
            shouldCloseOnOverlayClick={closeOnOverlayClick}
            closeTimeoutMS={duration}
            overlayClassName={styles.overlay}
            onRequestClose={onClose}
            className={classNames({
                [styles.modal]: true,
                [styles.modal_small]: size === 'small',
                [styles.modal_full]: size === 'fullscreen',
                [styles.modal_medium]: size === 'medium'
            })}
            ariaHideApp={false}
            style={{ overlay: { transitionDuration: `${duration}ms` } }}
        >
            <div className={styles.wrapper}>
                <div className={styles.header}>
                    <div className={styles.title}>
                        {title}
                    </div>
                    <div className={styles.close_button} onClick={onClose}>
                        Закрыть
                    </div>
                </div>
                <div className={styles.content}>
                    {children}
                </div>
            </div>
        </ReactModal>
    )
}

export default Modal