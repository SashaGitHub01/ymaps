import React, { useCallback, useEffect, useRef, useState } from 'react'
import { YMaps, Map, Placemark, FullscreenControl, ZoomControl, GeolocationControl, SearchControl, } from 'react-yandex-maps';
import styles from './YandexMapPick.module.scss'

const YandexMapPick = ({ center, mark, setMark, setPlace, setCenter, handleClick }) => {
    const [ymap, setYmap] = useState(null)

    const mapInit = (ymap) => {
        setYmap(ymap)
    }

    const getPlacemarkLayout = () => {
        // кастомный Placemark
        if (ymap) {
            return ymap.templateLayoutFactory.createClass(
                `<div class="${styles.placemark}">
                    <div class="${styles.circle}">
                    </div>
                </div>`,
            )
        }
    }

    const handleCreateMark = async (e) => {
        const coords = e.get('coords');
        if (!coords || !ymap) return;

        setMark(coords)

        const firstGeoObj = await geocodeCoords(coords);
        handleClick(firstGeoObj.getAddressLine())
        console.log('Адрес клика:', firstGeoObj.getAddressLine());
    }

    const geocodeCoords = async (coords) => {
        try {
            const geo = await ymap.geocode(coords, {});
            return geo.geoObjects.get('0');
        } catch (err) {

        }
    }

    useEffect(() => {
        if (ymap) {
            // Если в браузере разрешен доступ к геолокации юзера, то при инициализации центрируем и выставляем точку по локации пользователя
            ymap.geolocation.get({
                provider: 'browser',
                mapStateAutoApply: true
            }).then(async (result) => {
                if (result.geoObjects) {
                    const coords = result.geoObjects.position;
                    const location = await geocodeCoords(coords);
                    setPlace(location.getAddressLine())
                    setCenter(coords)
                    setMark(coords)
                }
            }).catch(err => console.log(err))

            // custom search input
            // const suggestView = new ymap.SuggestView('suggest', { results: 4 })
            // suggestView.events.add("select", async (e) => {
            //     // геокодируем выбранный item по значению и получаем координаты
            //     geocodeCoords(e.get('item').value)
            //         .then(res => {
            //             const coords = res.geometry.getCoordinates();
            //             setCenter(coords)
            //             setMark(coords)
            //         });
            // });
        }
    }, [ymap])

    return (
        <YMaps
            query={{
                apikey: 'e4963ec0-43ab-40c8-85ad-30043e1d83a7',
            }}
        >
            <Map
                onLoad={mapInit}
                modules={['geocode', "geoObject.addon.balloon", 'SuggestView', 'geolocation', "templateLayoutFactory", "layout.ImageWithContent"]}
                onClick={handleCreateMark}
                state={{ center, zoom: 17 }}
                options={{
                    suppressMapOpenBlock: true
                }}
                height='100vh'
                width="100%"
            >
                {Array.isArray(mark) && !!ymap
                    && <Placemark
                        geometry={mark}
                        options={{
                            // iconColor: 'orange',
                            // preset: 'islands#circleDotIcon',
                            iconLayout: getPlacemarkLayout(),
                            iconShape: {
                                type: 'Circle',
                                // Круг описывается в виде центра и радиуса
                                coordinates: [0, 0],
                                radius: 25
                            }
                        }}
                    />}
                <FullscreenControl />
                <ZoomControl />
                <GeolocationControl />
                {/* <input type="text" name="" id="suggest" /> */}
            </Map>
        </YMaps>
    )
}

export default YandexMapPick