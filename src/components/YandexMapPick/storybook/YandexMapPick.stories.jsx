import YandexMapPick from "../YandexMapPick"
import Modal from "../../Modal/Modal"
import { useState } from "react"
import { useArgs } from '@storybook/client-api'

export default {
    title: 'UI/YandexMap',
    size: ""
}

export const Playground = () => {
    const [center, setCenter] = useState([55.755819, 37.617644])
    const [mark, setMark] = useState(null)
    const [place, setPlace] = useState(null)
    const [open, setOpen] = useState(false)

    const onClose = () => {
        setOpen(false)
    }

    const handleClickMap = (place) => {
        setPlace(place)
        setOpen(true)
    }

    return (
        <div className="">
            <div className="App">
                <YandexMapPick
                    center={center}
                    mark={mark}
                    setMark={setMark}
                    setPlace={setPlace}
                    handleClick={handleClickMap}
                    setCenter={setCenter}
                />
            </div>
            {<Modal
                duration={300}
                open={open}
                onClose={onClose}
                title={"Когда приступить к работе?"}
                closeOnOverlayClick
            >
                <div>Click: {place}</div>
            </Modal>}
        </div>
    )
}
