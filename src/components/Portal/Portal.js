import React, { PropsWithChildren } from 'react'
import { createPortal } from 'react-dom';

const Portal = ({ children, isOpen }) => {
   return (
      <>
         {isOpen
            ? createPortal(children, document.body)
            : null}
      </>
   )
}

export default Portal;